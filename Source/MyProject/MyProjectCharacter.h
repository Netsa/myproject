// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MyProjectCharacter.generated.h"

UENUM(BlueprintType)
enum class EHangState : uint8
{
	HS_NotHanging			UMETA(DisplayName="Doesn't Hanging"),
	HS_Hanging				UMETA(DisplayName="Hanging"),
	HS_Climbing				UMETA(DisplayName="Climbing"),
	HS_MovingLedgeLeft		UMETA(DisplayName="Moving on Ledge Left"),
	HS_MovingLedgeRight		UMETA(DisplayName = "Moving on Ledge Right")
};

UCLASS(config=Game)
class AMyProjectCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AMyProjectCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:

	UFUNCTION(BlueprintCallable, Category = "Tracers")
	void LedgeTraceLeft(class UArrowComponent* ArrowComp);

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	void MovePlayerRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	FVector ForwardStartLocation();

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	FVector ForwardEndLocation();

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	FVector HeightStartLocation();

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	FVector HeightEndLocation();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Variables")
	FHitResult ForwardHitResult;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Variables")
	FHitResult HeightHitResult;

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	bool HipToLedge(USkeletalMeshComponent* PlayerMesh, FName SocketName);

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	FVector MovePlayerCapsule();

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	FRotator RotatePlayerCapsule();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Variables")
	FName PelvisSocketName;

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	void MovePlayerForward(float Value);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Enums")
	EHangState HangState;

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	FVector ArrowLocationComponent(class UArrowComponent* ArrowComp);

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	FVector ArrowLocationComponentTurnStart(class UArrowComponent* ArrowComp);

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	FVector ArrowLocationComponentTurnEnd(class UArrowComponent* ArrowComp);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Variables")
	bool IsMovableLeft;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Variables")
	bool IsMovableRight;

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	void MoveOnLedgeRight(float MoveRight);

	UFUNCTION(BlueprintCallable, Category = "GameplayMaths")
	void MoveOnLedgeLeft(float MoveRight);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Variables")
	bool IsMovingRightOnLedge;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Variables")
	bool isMovingLeftOnLedge;

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

	virtual void BeginPlay() override;


public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

