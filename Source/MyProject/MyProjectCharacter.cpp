// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
//TODO: @Netsa add jump function between two ledges

#include "MyProjectCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Components/ArrowComponent.h"
#include "DrawDebugHelpers.h"

//////////////////////////////////////////////////////////////////////////
// AMyProjectCharacter

AMyProjectCharacter::AMyProjectCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	//------------------Set In-Game variables--------------------

	PelvisSocketName = "PelvisSocket";

	//------------------------------------------------------------
}

bool AMyProjectCharacter::HipToLedge(USkeletalMeshComponent* PlayerMesh, FName SocketName)
{
	if(PlayerMesh->GetSocketLocation(SocketName).Z - HeightHitResult.Location.Z >= -30.f && PlayerMesh->GetSocketLocation(SocketName).Z 
		- HeightHitResult.Location.Z <= 0.f)
	{
		return true;
	}
	 else return false;
}

FVector AMyProjectCharacter::MovePlayerCapsule()
{
	return FVector((ForwardHitResult.Normal.X * 39.f) + ForwardHitResult.Location.X, ForwardHitResult.Normal.Y * 39.f 
		+ ForwardHitResult.Location.Y, HeightHitResult.Location.Z - 100.f);
}

FRotator AMyProjectCharacter::RotatePlayerCapsule()
{
	return ForwardHitResult.Normal.Rotation() - FRotator(0.f, 180.f, 0.f);
}

void AMyProjectCharacter::MovePlayerForward(float Value)
{
	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0);
	FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

	AddMovementInput(Direction, Value);
}

FVector AMyProjectCharacter::ArrowLocationComponent(UArrowComponent* ArrowComp)
{
	return ArrowComp->GetComponentLocation();
}

FVector AMyProjectCharacter::ArrowLocationComponentTurnStart(class UArrowComponent* ArrowComp)
{
	return FVector(ArrowComp->GetComponentLocation().X, ArrowComp->GetComponentLocation().Y, ArrowComp->GetComponentLocation().Z + 60.f);
}

FVector AMyProjectCharacter::ArrowLocationComponentTurnEnd(class UArrowComponent* ArrowComp)
{
	return FVector(ArrowLocationComponentTurnStart(ArrowComp) + (ArrowComp->GetForwardVector() * 70.f));
}

void AMyProjectCharacter::MoveOnLedgeRight(float MoveRight)
{
	if (IsMovableRight == true)
	{
		if (MoveRight > 0.f)
		{
			FVector NewActorLocation = GetActorRightVector() * 20.f + GetActorLocation();
			FVector ActorLocation = FMath::VInterpTo(GetActorLocation(), NewActorLocation, GetWorld()->GetDeltaSeconds(), 5.f);
			SetActorLocation(ActorLocation);
			isMovingLeftOnLedge = false;
			IsMovingRightOnLedge = true;
			HangState = EHangState::HS_MovingLedgeRight;
		}
	}
}

void AMyProjectCharacter::MoveOnLedgeLeft(float MoveRight)
{
	if (IsMovableLeft == true)
	{
		if (MoveRight < 0.f)
		{
			FVector NewActorLocation = GetActorRightVector() * -20.f + GetActorLocation();
			FVector ActorLocation = FMath::VInterpTo(GetActorLocation(), NewActorLocation, GetWorld()->GetDeltaSeconds(), 5.f);
			SetActorLocation(ActorLocation);
			isMovingLeftOnLedge = true;
			IsMovingRightOnLedge = false;
			HangState = EHangState::HS_MovingLedgeLeft;
		}
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void AMyProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMyProjectCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMyProjectCharacter::LookUpAtRate);
}

void AMyProjectCharacter::BeginPlay()
{
	Super::BeginPlay();
}

void AMyProjectCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMyProjectCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

FVector AMyProjectCharacter::ForwardStartLocation()
{
	return GetActorLocation();
}

FVector AMyProjectCharacter::ForwardEndLocation()
{
	return GetActorLocation() + (GetActorRotation().Vector() * FVector(150.f, 150.f, 0.f));
}

void AMyProjectCharacter::LedgeTraceLeft(class UArrowComponent* ArrowComp)
{
	UWorld* World = GetWorld();
	if (World)
	{
		FHitResult HitResult;
		FCollisionShape Shape;
		Shape.Capsule;

		if (World->SweepSingleByChannel(HitResult, ArrowComp->GetComponentLocation(), ArrowComp->GetComponentLocation(), ArrowComp->GetComponentQuat(), ECC_GameTraceChannel1, Shape))
		{

		}
	}
}

void AMyProjectCharacter::MovePlayerRight(float Value)
{
	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0, Rotation.Yaw, 0); 
	const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

	AddMovementInput(Direction, Value);
}

FVector AMyProjectCharacter::HeightStartLocation()
{
	return GetActorLocation() + FVector(0, 0, 500.f) + (GetActorRotation().Vector() * 70);
}

FVector AMyProjectCharacter::HeightEndLocation()
{
	return HeightStartLocation() - FVector(0.f, 0.f, 500.f);
}